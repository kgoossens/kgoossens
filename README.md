# Kristof Goossens's Readme

## Personality 

### Meyer's Briggs

- **🤹 Personality**: [ESFP-T](https://www.16personalities.com/profiles/5940c54aa7f9f) (Extraverted, Observant, Feeling, Prospecting, Turbulent): Turbulent Entertainer - Entertainers are spontaneous, energetic, and enthusiastic people – life is never boring around them. They love vibrant experiences, eagerly engaging in life and discovering the unknown with others.
- **🌋 Role** : [Explorer](https://www.16personalities.com/articles/roles-explorers) - Explorers are utilitarian, practical, and spontaneous, shining in situations that require quick reaction and ability to think on their feet. They are masters of tools and techniques.
- **🤝 Strategy**: [Social Engagement](https://www.16personalities.com/articles/strategies-social-engagement) - Social Engagers are restless, perfectionistic people, prone to experiencing very positive and negative emotions. They’re curious and willing to work hard, making them high-achievers.


## Values

GitLab's [CREDIT](https://handbook.gitlab.com/handbook/values/#credit) Values is a big part of why I joined.

If I do need to chose any of the Values that would define me it would be mainly:

- Collaboration: I like working together and joining forces to reach our Goal. *Together* with my coworkers, customers and prospects **and** the wider GitLab community.
- Transparency: public by default. To the very extreme... well perhaps not on Instagram ;-) 

## Communication Style

Communication is key to success of anything that involves more then 1 person working on it. Therefore I have multiple ways that I like to communicate:

- Quick Questions go to Slack, preferably in a public channel
- Personal, or privacy sensitive questions can of course go in DM or be discussed during meetings
- Product related changes/questions : Issue first wherever possible
- When in doubt, overcommunicate!

Keep in mind that English is my secondary language, so some subtle references might escape me. If it does, be direct about it. [Radical Candor](https://en.wikipedia.org/wiki/Radical_Candor) works for me, it's even preferred.

That also means feedback is welcome, feel free to tell me directly, or send [360 feedback](https://about.gitlab.com/handbook/business-technology/tech-stack/#cultureamp) my way through CultureAmp.

### ☕ Coffee Chats

From my first day at GitLab, until now, I'm part of the [#donut-be-strangers](https://about.gitlab.com/company/culture/all-remote/informal-communication/#the-donut-bot)-channel.

But if we aren't matched there, feel free to [schedule a coffee chat](https://app.reclaim.ai/m/kristof/coffee-chat) directly, my calendar is open.

## Events

### Bio

Kristof is solution-driven. From his background in Software Testing and Quality Assurance, he knows that there are no problems, only challenges, and 'unexplained features'. For that same reason, he loves working with the community members to address these challenges. Outside of work, you'll find me entertaining my kids, volunteering as a CoderDojo Coach, which gives me an excuse to play with Arduino's, Raspberry Pi's, and other fun stuff to keep my inner child satisfied.

### Pictures

- Picture with [Transparent Background](https://gitlab.com/kgoossens/kgoossens/-/blob/7b4f89ab0b27e54c519dc1b65ecb6d9aabd6f4a3/media/kristof_transp.png)
- 400x400 Picture in [Color](https://gitlab.com/kgoossens/kgoossens/-/blob/7b4f89ab0b27e54c519dc1b65ecb6d9aabd6f4a3/media/kgoossens_400x400_ktch.png)
- [Grayscale Picture](https://gitlab.com/kgoossens/kgoossens/-/blob/7b4f89ab0b27e54c519dc1b65ecb6d9aabd6f4a3/media/kgoossens_pic_bigger.png)

## Get in touch

- 🦣 Mastodon: https://fosstodon.org/@kgoossens
- 🔗 LinkedIn: https://www.linkedin.com/in/goossensk/
- 🦊 Gitlab.com: http://gitlab.com/kgoossens (⚠️ spoiler alert: you are here!)
